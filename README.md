To run this project just go to the directory you have and it cloned /path/to/the/project and type mvn test in the command line or run it in the IDE if you have the project setup.

This is a hello world program for a cucumber test. Do not just run this and think you have mastered cucumber testing but actually go through how you can write
different Scenarios with a lot of examples.

This is nothing but a simple hello world program which does not go into much detail as to what is happening. If you have tough time doing this. Please follow the link below.

https://cucumber.io/docs/reference/jvm#lambda-expressions-java-8

https://cucumber.io/training

Please let me know if you have any questions, you know where to find me :D